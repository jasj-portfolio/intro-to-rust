# Intro-To-Rust


## Name
Intro to Rust

## Description
This repo is where I will be taking my notes as I learn Rust fundamentals, and beyond when I start to dive deeper into the language.
- I took my notes in a rust file so when I did a few example functions I would have syntax highlighting.

## Installation
Will have a couple small programs (cli probably) but mostly notes. So all you have to do is clone the repo.
- git clone https://www.gitlab.com/jasj-portfolio/intro-to-rust.git

## License
Since I will be adding my practice programs to track progress will be using Gnu/GPLv3


