// These are my notes on Rust following Tech With Tim on YouTube.
// The notes are written in a way that makes it easier for me to understand.
// Feel free to use these notes as is or reformat them in a way that suites you
// Will grammar and spell check notes after I complete the playlist.
// My first few practice programs will also be in this repo
// Think you for learning with me.  --Johnny Scott

// My Hello World program
fn  main() { // fn defines the function like def in python main is the executed block of code like in go-lang
    println!("Hello World!"); // println! is a macro desginated by the <!> exclamtion point, they are defferent then functions. Lines end in semicolons <;>
}

// How to compile Rust
// - rustc <program name>
// - no need to worry about .pdb file

// cargo
// - initialize with cargo
//     - cargo new <project name>
// - compile with cargo
//     - cargo build (used inside project directory)
//     - cargo run (same use case as build compiles and runs project)
// - debug
//     - cargo check

// rustfmt
// - format code
//     - cd to directory of the .rs file
//     - rustfmt <project name>.rs

// crates and modules 
// - use: rusts version of import
//     - crate: is a library
//     - module: a functionality from a crate
// - syntax use <crate>::<module>;
//     - example use std::io;
//     - that would be equivalent to from std import io in python

// variables
// - can reuse variable names by redefining with the let keyword
// - variables are immutable by default
//     - let mut x = <value>; keyword mut lets us change the value in the variable
// - implicit variables
//     - compilier checks and sets variable type when compiled
//     - example let x = 4; <== compilier will set type to integar
//     - can not change type of variable name
// - explicit variables
//     - example let x: <type> = <value>;
// - name shadowing: using two variables from different scopes with the same name
//     - interior scopes can use variables from exterior scopes but not the other way around
//     - similar to how variables can be used inside functions with python

// constants
// - keyword is const
//     - example const <constant name>
//     - constant name is in all caps seperated with underscores
//     - have to specify a type and use a colon similar to variables
//         - example const THIS_IS_A_CONSTANT: u32 = <value>;
//     - can not be redefined like variables

// strings
// - formated string
//     - example println!("x is {}", <variable>); <== variable goes outside of brackets and quotes

// data types
// - bit range for integers 8, 16, 32, 64, 128
// - bit range for floating numbers 32, 64
// - boolean is true or false all lowercase (This will be hard for me to remember coming from Python...)
// - char one character and single quotes only
// - when assigning bits 32 is the default for integers
// - primitive data types
//     - scalar: single value
//         - example integer let x: i32 = 2;
//             - i represents integer and 32 represents the bits used to store the number
//         - integer: i
//         - unsigned integer: u (can't be negetive)
//         - floating number: f
//         - boolean: bool
//         - character: char
//     - compound: multiple values
//         - tuple: marked by scalar types inside of tuple in parentheses
//             - example let tuple1: (i32, bool, char) = (1, true, 's');
//             - indexing a tuple example println!("{}", tuple1.<indexing number>) or println!("{}", tuple1.0)
//             - tuples are mutable if you use the mut keyword
//                 - example let mut tup: (i32, bool) = (3, false);
//                           tup.1 = true;
//         - arrays: same rules as tuples with a couple exceptions
//             - use [] instead of ()
//             - only one data type alloud 
//             - type set with data type in array  semicolon number of elements in array
//                 - example let arr: [i32; 5] = [1, 2, 3, 4, 5];
//             - index with [] instead of .
//                 - example println!("{}", arr[4]);
// - type conversion
//     - example let x = 16;
//               let y: i8 = 19;
//               let z = x + y as i32; <== as i32 converts y's data type from i8 to i32
// - type casting
//     - examples
//         - 32i8
//         - 32_i8
//         - 32 as i8

// - user input (example w/ syntax highlighting at end of notes section)
//     - to get user input you have to use the io module from the std crate/library
//     - first you set a mutable variable then assign an empty string
//     - String::new(): sets variable to empty string
//     - io::stdin().read_line(&mut input) collects standard input and references the variable to set the new value to users input 
//     - .expect() error handling not mandatory but good practice
//     - .trim() removes any input recieved from terminal application
//     - .parse() converts string input to another data type 
//         - example "123" to 123
//     - .unwrap() similar to expect for integers

// user input example 
use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("failed to read line"); // collects user inputs
    let int_input: i64 = input.trim().parse().unwrap(); // changes string input into integers
    println!("{}", input + 2); // prints user input to screen
}

// math
// - if a number is outside of a bit range will throw an error
//     - example u8 has a range of 0 - 255 so assigning 256 or higher will throw
//     - the result of an arithmetic problem can exceed the byte range of the numbers in the problem
//         - example let x: u8 = 254;
//                   let y: u8 = 13;
//         - this would throw because 267 is greater then 255
// - arithmetic operators
//     - / = division (make sure to use float type to get float result)
//     - + = addition
//     - * = multiplication
//     - - = subtraction
//     - % = mod (to see remainder after division)

// conditions
// - syntax: let cond = 2 < 3;
// - must use same data types in comparisons
// - comparisons
//     - <: less then
//     - >: greater then
//     - <=: less then or equal too
//     - >=: greater then or equal too
//     - !=: not equal too
//     - ==: equal too
// - logical operators
//     - &&: and
//     - ||: or
//     - !: not

// else if statement
use std::io;

fn main(){
    let mut name = String::new();
    io::stdin().read_line(&mut name).expect("could not read input");
    if name == "johnny"{
        println!("Hello");
    } else if name != "johnny"{
        println!("these ain't your notes");
    } else{
        println!("you fucked up somewhere");
    }
}

// functions
// - calling a function is alot like python, setting up function is same as rust's main function
// - param data types have to specified
// - you have to specify return data type
// statements v. expressions
// - statements are stuff like variable declarations, they dont return anything
// - expressions are everything else that evaluates to something

// simple function that returns an int
fn add_numbers(x: i64, y: i64) -> i64{
    x + y // no semicolon because it is an expression not a statement, if you use return keyword you can use semicolon 'return x + y;'
}

fn main(){
    let results = add_numbers(1000, 337);
    println!("{}", results);
}